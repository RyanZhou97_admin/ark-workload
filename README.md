# ArkCompiler WorkLoad

### Introduction

Performance test cases, used to test the performance of the ark compiler
1) ts workload
2) swift workload
3) napi workload

### Running tests
1) DownLoads the project to your OpenHarmony env root
2) `chmod +x run_aot.sh`
3) build your project with build-target as rk3568
4) running script with command `bash run_aot.sh`
5) Waiting for results :) (It would be a good choice to output it to a file to check results `bash run_aot.sh > output.txt`)

### ts-swift-workload notes
#### fannkuc-redux Benchmark
  - https://benchmarksgame-team.pages.debian.net/benchmarksgame/description/fannkuchredux.html#fannkuchredux
  - target input is 12 => decrease number to decrease difficulty of the benchmark

#### fasta Benchmark
  - https://benchmarksgame-team.pages.debian.net/benchmarksgame/description/fasta.html#fasta
  - target input is 25000000 => decrease number to decrease difficulty of the benchmark

#### mandelbrot Benchmark
  - https://benchmarksgame-team.pages.debian.net/benchmarksgame/description/mandelbrot.html#mandelbrot
  - target input is 16000 => decrease number to decrease difficulty of the benchmark

#### spectralnorm Benchmark
  - https://benchmarksgame-team.pages.debian.net/benchmarksgame/description/spectralnorm.html#spectralnorm
  - target input is 5500 => decrease number to decrease difficulty of the benchmark
