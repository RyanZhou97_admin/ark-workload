#! /bin/bash
if [ $# -eq 0 ]; then
    benchmark="ALL"
    echo  "all"
else
    benchmark=$1
fi
benchmarkALL="ALL"
swift_path=ts-swift-workload
swift_files=""
LogDate=$(date '+%Y%m%d%H%M%S')
result_file_path=result/$LogDate
function read_dir(){
    if [ "$benchmark" = "$benchmarkALL" ]; then
        if [ ! -d $result_file_path ];then
            mkdir -p $result_file_path
        fi
        # echo  "----- Run $benchmark Benchmark Case -----" >> result/benchmark_result$LogDate.log
        for file in `ls $swift_path`
        do
            if [ -d $swift_path"/"$file ]
            then
                if [ -d $swift_path/$file/swift ]
                then
                    for file1 in ${swift_path}/${file}/swift/*
                    do
                        filename=$(basename "$file1")
                        if [ ! -d $result_file_path/$file ]; then
                            mkdir -p $result_file_path/$file
                        fi
                        filename="${filename%.*}"
                        swiftc -O -whole-module-optimization $file1 -o $result_file_path/$file/$filename
                    done
                fi
        fi
        done
    else
        if [ "$benchmark" = "--help" ]; then
            echo run all test files by default when there are no parameters
            echo [ALL]             run all swift test suites
            echo [path]              run the test suite in the path
        else
            if [ -f $benchmark ];then
                filename=$(basename "$benchmark")
                filename="${filename%.*}"
                if [ ! -d $result_file_path/$filename ];then
                    mkdir -p $result_file_path/$filename
                fi
                echo "Run $filename Case"  >> $result_file_path/${filename}-result.log
                if [ -f $result_file_path/$filename/$filename ]
                then
                    rm $result_file_path/$filename/$filename
                fi
                swiftc  -O -whole-module-optimization  $benchmark -o $result_file_path/$filename/$filename
                if [ -f $result_file_path/$filename/$filename ]
                then
                    $result_file_path/$filename/$filename  >> $result_file_path/${filename}-result.log
                    # rm $result_file_path/$filename/$filename
                fi
            fi
        fi
    fi


} 
read_dir $swift_path