
declare -A testSets
# testSets["arrayAccess"]="./ts-swift-workload/array-access/ts/"
# testSets["fannkuc-redux"]="./ts-swift-workload/array-access/ts/"
# testSets["fasta"]="./ts-swift-workload/array-access/ts/"
# testSets["mandelbrot"]="./ts-swift-workload/array-access/ts/"
# testSets["spectralnorm"]="./ts-swift-workload/array-access/ts/"
# testSets["threeDCube"]="./ts-swift-workload/array-access/ts/"
# testSets["threeDRaytrace"]="./ts-swift-workload/array-access/ts/"
# # testSets["Deep_Nested_Call"]="./ts-swift-workload/function-call/ts/"
# testSets["Method_Call"]="./ts-swift-workload/function-call/ts/"
# testSets["Normal_Call"]="./ts-swift-workload/function-call/ts/"
# testSets["Pointer_Call"]="./ts-swift-workload/function-call/ts/"
# # testSets["Shallow_Nested_Call"]="./ts-swift-workload/function-call/ts/"
# testSets["nbody"]="./ts-swift-workload/nbody/ts/"
# testSets["cocos"]="./ts-swift-workload/cocos/ts/"
# testSets["cocos_airplane"]="./ts-swift-workload/cocos/ts/"
# testSets["bitops"]="./ts-swift-workload/numerical-calculation/ts/"
# testSets["cordic"]="./ts-swift-workload/numerical-calculation/ts/"
testSets["numericalComputation"]="./ts-swift-workload/numerical-calculation/ts/"
testSets["partialSum"]="./ts-swift-workload/numerical-calculation/ts/"
# testSets["propertyaccess1"]="./ts-swift-workload/property-access/ts/"
# testSets["propertyaccess2"]="./ts-swift-workload/property-access/ts/"
# testSets["splay"]="./ts-swift-workload/splay/ts/"

read -p "is_standalone? true or false: " choice
echo $choice
sdk_path="/home/xfli/workerspace/openharmony/code"
abc2path="abc2path"
True="true"
False="false"

if [ ! -d $abc2path ];then
    mkdir -p $abc2path
fi

if [ "${choice,,}" = ${False,,} ] ; then
    echo choice is ${choice,,}
	read -p "Choose your target from [rk3568, hispark_taurus_standard]: " target
	if [[ "$target" == "rk3568" || "$target" == "hispark_taurus_standard" ]]; then
        $sdk_path/out/${target}/clang_x64/arkcompiler/ets_frontend/es2abc $sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.ts --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output $sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc

        $sdk_path/out/${target}/clang_x64/arkcompiler/ets_frontend/es2abc ./utils/assert.ts --merge-abc --module --output ./utils/assert.abc
        $sdk_path/out/${target}/clang_x64/arkcompiler/ets_frontend/es2abc ./utils/benchmarkTsSuite.ts --merge-abc --module --output ./utils/benchmarkTsSuite.abc

        export LD_LIBRARY_PATH=$sdk_path/out/${target}/clang_x64/lib.unstripped/clang_x64/arkcompiler/ets_runtime:$sdk_path/out/${target}/clang_x64/lib.unstripped/clang_x64/test/test:$sdk_path/out/${target}/clang_x64/lib.unstripped/clang_x64/thirdparty/icu:prebuilts/clang/ohos/linux-x86_64/llvm/lib:$sdk_path/out/${target}/clang_x64/lib.unstripped/clang_x64/thirdparty/zlib

        for key in ${!testSets[@]};do
            ts_file=${testSets[${key}]}${key}.ts
            file_name=${key}
            abc_file=${testSets[${key}]}${key}.abc
            echo ${ts_file}
            echo ${file_name}
            echo ${abc_file}

            $sdk_path/out/${target}/clang_x64/arkcompiler/ets_frontend/es2abc ${ts_file} --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output ${abc_file}

            $sdk_path/out/${target}/clang_x64/exe.unstripped/clang_x64/arkcompiler/ets_runtime/ark_aot_compiler --builtins-dts=$sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc --aot-file=${file_name} ${abc_file}

            $sdk_path/out/${target}/clang_x64/exe.unstripped/clang_x64/arkcompiler/ets_runtime/ark_js_vm --icu-data-path "$sdk_path/third_party/icu/ohos_icu4j/data" --log-level=info --asm-interpreter=true --entry-point=${file_name} --aot-file=${file_name} ${abc_file}

        done

	else
		echo "Invalid target, please choose in [rk3568, hispark_taurus_standard]"
    fi
else
    echo choice is ${choice,,}
    target="x64.release"
    for key in ${!testSets[@]};do
        ts_file=${testSets[${key}]}${key}.ts
        file_name=${key}
        abc_file=$abc2path/${file_name}/${file_name}.abc
        echo ${file_name}
        if [ ! -d $abc2path/$file_name ];then
            mkdir -p $abc2path/$file_name
        fi
        
        $sdk_path/out/$target/exe.unstripped/arkcompiler/ets_frontend/es2abc $sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.ts --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output $sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc
        export LD_LIBRARY_PATH=$sdk_path/out/$target/lib.unstripped/arkcompiler/ets_runtime:$sdk_path/out/$target/lib.unstripped/test/test:$sdk_path/out/$target/lib.unstripped/thirdparty/icu:$sdk_path/prebuilts/clang/ohos/linux-x86_64/llvm/lib:$sdk_path/out/$target/lib.unstripped/thirdparty/zlib
        $sdk_path/out/$target/exe.unstripped/arkcompiler/ets_frontend/es2abc ./utils/benchmarkTsSuite.ts --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output $abc2path/$file_name/benchmarkTsSuite.abc
        $sdk_path/out/$target/exe.unstripped/arkcompiler/ets_frontend/es2abc ./utils/assert.ts --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output $abc2path/$file_name/assert.abc
        $sdk_path/out/$target/exe.unstripped/arkcompiler/ets_frontend/es2abc ${ts_file} --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output $abc2path/$file_name/${file_name}.abc
        $sdk_path/out/$target/exe.unstripped/arkcompiler/ets_runtime/ark_js_vm --icu-data-path "$sdk_path/third_party/icu/ohos_icu4j/data" --enable-pgo-profiler=true --compiler-pgo-profiler-path=./$abc2path/$file_name --log-level=info --asm-interpreter=true --entry-point=${file_name} ${abc_file}
        $sdk_path/out/$target/exe.unstripped/arkcompiler/ets_runtime/ark_aot_compiler --log-level=info --builtins-dts=$sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc --compiler-pgo-profiler-path=./$abc2path/$file_name/modules.ap --aot-file=./$abc2path/$file_name/${file_name} ${abc_file}
        $sdk_path/out/$target/arkcompiler/ets_runtime/ark_js_vm --icu-data-path "$sdk_path/third_party/icu/ohos_icu4j/data" --log-level=info --asm-interpreter=true --entry-point=${file_name} --aot-file=./$abc2path/$file_name/${file_name} ${abc_file}
    done
fi



