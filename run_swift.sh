#! /bin/bash
if [ $# -eq 0 ]; then
    benchmark="ALL"
    echo  "all"
else
    benchmark=$1
fi
benchmarkALL="ALL"
swift_path=ts-swift-workload
swift_files=""
LogDate=$(date '+%Y%m%d%H%M%S')
result_file_path=result/$LogDate
function read_dir(){
    if [ "$benchmark" = "$benchmarkALL" ]; then
        if [ ! -d $result_file_path ];then
            mkdir -p $result_file_path
        fi
        # echo  "----- Run $benchmark Benchmark Case -----" >> result/benchmark_result$LogDate.log
        for file in `ls $swift_path`
        do
            if [ -d $swift_path"/"$file ]
            then
                if [ -d $swift_path/$file/swift ]
                then
                    swift_files="${swift_path}/${file}/swift/*.swift"
                    if [ -f run ]
                    then
                        rm run
                    fi
                    swiftc ./$swift_files utils/*.swift  main.swift -o run
                    if [ -f run ]
                    then
                        echo  "----- Run ${file} Benchmark Case -----" >> $result_file_path/${file}-result.log
                        ./run >> $result_file_path/${file}-result.log
                        rm run
                    fi
                fi
        fi
        done
    else
        if [ "$benchmark" = "--help" ]; then
            echo run all test files by default when there are no parameters
            echo [ALL]             run all swift test suites
            echo [path]              run the test suite in the path
        else
            if [ -d ts-swift-workload/$benchmark ];then
                if [ ! -d $result_file_path ];then
                    mkdir -p $result_file_path
                fi
                echo "Run $benchmark Case"  >> $result_file_path/${benchmark}-result.log
                swift_files="${swift_path}/${benchmark}/swift/*.swift"
                if [ -f run ]
                then
                    rm run
                fi
                swiftc ./$swift_files utils/*.swift  main.swift -o run
                if [ -f run ]
                then
                    ./run  >> $result_file_path/${benchmark}-result.log
                    rm run
                fi
            fi
        fi
    fi


} 
read_dir $swift_path