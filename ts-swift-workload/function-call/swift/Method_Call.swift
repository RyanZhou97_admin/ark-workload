import Glibc
class Timer {
    private let CLOCK_REALTIME = 0
    private var start_timespec = timespec()
    private var end_timespec = timespec()
    private var time_spec = timespec()
    func start() {
        clock_gettime(Int32(CLOCK_REALTIME),&start_timespec)
    }
    
    func stop() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&end_timespec)
        let start_time = Double(start_timespec.tv_sec * 1_000_000 + start_timespec.tv_nsec / 1_000)
        let end_time = Double(end_timespec.tv_sec * 1_000_000 + end_timespec.tv_nsec / 1_000)
        let time = end_time - start_time
        return time / 1_000
    }
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}
func GenerateRandoms() -> [Int]{
    let result = Array(repeating: Int.random(in: 1..<3), count: 2)
    return result
}
var generaterandoms = GenerateRandoms()

func TimeGenerateRandoms() -> Double{
    let timer = Timer()
    timer.start()
    let tmp = 1
    if generaterandoms[tmp%2] != 0 {

    }
	let time = timer.stop()
    return time
}

// @inline(__always)
var global = 0

func GenerateFakeRandomInteger() -> [Int] {
    let resource: [Int] = [12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]
    return resource;
}

class ClassFunc {
   @inline(never)
    func foo(resources: [Int], i: Int, i3: Int, resourcesLength: Int) ->Int {
        var i4 = i3;
        if ((resources[i % Int(i3) & (resourcesLength - 1)] & 1) == 0) {
            i4 += 1;
        } else {
            i4 += 2;
        }
        return i4;
    }
    @inline(never)
    func parameterlessFoo() {
        global += 1
    }
    @inline(never)
    func differentFoo(_ a: Int, _ b: String, _ c: Bool){
        global += 1
    }
    @inline(never)
    func defaultFoo(_ a: Int = 0, _ b: String = "b", _ c: Bool = true) {
        global += 1
    }
    @inline(never)
    func variableFoo(_ a: Int? = nil, _ b: String? = nil, _ c: Bool? = nil) {
        global += 1
    }
    @inline(never)
    func argsFoo(_ args: Int...) {
        global += 1
    }
}
/***************** With parameters *****************/
// Method call

func runMethodCall() -> Int {
    let count = 10000000
    let cf = ClassFunc()
    let timer = Timer()
    let resources = GenerateFakeRandomInteger();
    var i3 = 1
    timer.start()
    let func1:([Int], Int, Int, Int) -> Int = cf.foo
    let resourcesLength = resources.count
    for i in 0..<count {
        i3 = func1(resources, i, i3, resourcesLength)
    }
	let time = timer.stop()
    print("Function Call - RunMethodCall:\t"+String(time)+"\tms");
    return Int(time)
}
_ = runMethodCall()

func runParameterlessMethodCall() -> Int {
    let count = 10000000
    let cf = ClassFunc()
    let timer = Timer()
    global = 0
    timer.start()
    for _ in 0..<count {
        cf.parameterlessFoo()
    }
	let time = timer.stop()
    print("Function Call - RunParameterlessMethodCall:\t"+String(time)+"\tms");
    return Int(time)
}
_ = runParameterlessMethodCall()
/***************************** Default  parameters *****************************/
// Method call
func runDefMethodCall() -> Int {
    let count = 10000000
    let cf = ClassFunc()
    let timer = Timer()
    global = 0
    timer.start()
    for _ in 0..<count {
        cf.defaultFoo()
    }
	let time = timer.stop()
    print("Function Call - RunDefMethodCall:\t"+String(time)+"\tms");
    return Int(time)
}
_ = runDefMethodCall()
/********************* Different  parameters *********************/
// Method call
func runDifMethodCall() -> Int {
    let count = 10000000
    let cf = ClassFunc()
    let timer = Timer()
    global = 0
    timer.start()
    for _ in 0..<count {
        cf.differentFoo(1, "1", true)
    }
	let time = timer.stop()
    print("Function Call - RunDifMethodCall:\t"+String(time)+"\tms");
    return Int(time)
}
_ = runDifMethodCall()
/************************* Variable  parameters *************************/
// Method call
func runVariableMethodCall() -> Int {
    let count = 10000000
    let cf = ClassFunc()
    let timer = Timer()
    global = 0
    timer.start()
    for _ in 0..<count {
        cf.variableFoo(1, "1", true)
    }
	let time = timer.stop()
    print("Function Call - RunVariableMethodCall:\t"+String(time)+"\tms");
    return Int(time)
}
_ = runVariableMethodCall()
/***************************** ...Args  parameters *****************************/
// Method call
func runArgsMethodCall() -> Int {
    let count = 10000000
    let cf = ClassFunc()
    let timer = Timer()
    global = 0
    timer.start()
    for _ in 0..<count {
        // global += 1
        cf.argsFoo(1)
    }
	let time = timer.stop()
    print("Function Call - RunArgsMethodCall:\t"+String(time)+"\tms");
    return Int(time)
}


print("Swift Method Call Is End, global value : \(global)")