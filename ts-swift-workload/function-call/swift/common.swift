let MAXSIZE = 1000000

class ClassFunc {
    func foo(_ a: Int, _ b: Int, _ c: Int) {
    }
    
    func parameterlessFoo() {
    }
    
    func differentFoo(_ a: Int, _ b: String, _ c: Bool) {
    }
    
    func defaultFoo(_ a: Int = 0, _ b: String = "b", _ c: Bool = true) {
    }
    
    func variableFoo(_ a: Int? = nil, _ b: String? = nil, _ c: Bool? = nil) {
    }
    
    func argsFoo(_ args: Int...) {
    }
}
/***************** With parameters *****************/
func foo(_ a: Int, _ b: Int, _ c: Int) {
}
func callFoo(_ f: (Int, Int, Int) -> Void, _ a: Int, _ b: Int, _ c: Int) {
    f(a, b, c)
}
/***** Without parameters *****/
func parameterlessFoo() {
}
func callParameterlessFoo(_ f: () -> Void) {
    f()
}
/***************************** Default  parameters *****************************/
func defaultFoo(_ a: Int! = 0, _ b: String! = "b", _ c: Bool! = true) {
}
func callDefaultFoo(_ f: (Int?, String?, Bool?) -> Void, _ a: Int = 0, _ b: String = "b", _ c: Bool = true) {
    f(nil,nil,nil)
}
/********************* Different  parameters *********************/
func differentFoo(_ a: Int, _ b: String, _ c: Bool) {
}
func callDifferentFoo(_ f: (Int, String, Bool) -> Void, _ a: Int, _ b: String, _ c: Bool) {
    f(a, b, c)
}
/************************* Variable  parameters *************************/
func variableFoo(_ a: Int?, _ b: String?, _ c: Bool?) {
}
func callVariableFoo(f: (Int?, String?, Bool?) -> Void, _ a: Int?, _ b: String?, _ c: Bool?) {
    f(a, b, c)
}
/***************************** ...Args  parameters *****************************/
func argsFoo(_ args: Int...) {
}
func callArgsFoo(_ f: (Int...) -> Void, _ args: Int...) {
    f(args[0], args[1], args[2], args[3], args[4], args[5])
}