
// import Glibc
// let MAXSIZE = 10000000

// func runBuildInCall()->Int{
    
//     let timer = Timer()
//     global = 0
//     timer.start()
//     for i in 0..<MAXSIZE {
//         let reversed = "Run BuildIn Call ".reversed()
//         let minVal = min(10, 100)
//         let maxVal = max(10, 100)
//         let absNum = abs(-200)
//         let roundedPi = round(3.14159)
//         let sqrtNum = sqrt(Double(25))
//         let sinVal = sin(Double.pi / 4)
//         let cosVal = cos(Double.pi / 3)
//         let sortedNumbers = [10,15.1,158,41,6551,12].sorted()
//         let reversedLetters = ["a", "b", "c", "d"].reversed()
//     }
//     let time = timer.stop()
//     return Int(time)
    
// }
// //复杂的buildin构建
// struct CommandLineArgument {
//     let option: String?
//     let value: String?
// }
// class CommandLineArgumentsParser {
//     func parseCommandLineArguments() -> [CommandLineArgument] {
//         var parsedArguments = [CommandLineArgument]()
        
//         let arguments = CommandLine.arguments
        
//         for index in 1..<arguments.count {
//             let argument = arguments[index]
//             if argument.hasPrefix("-") {
//                 let option = String(argument.dropFirst())
//                 if index + 1 < arguments.count && !arguments[index + 1].hasPrefix("-") {
//                     let value = arguments[index + 1]
//                     parsedArguments.append(CommandLineArgument(option: option, value: value))
//                 } else {
//                     parsedArguments.append(CommandLineArgument(option: option, value: nil))
//                 }
//             }
//         }
        
//         return parsedArguments
//     }
// }

// class TableProcessor {
//     private var table: [[String]]
    
//     init(initialTable: [[String]]) {
//         self.table = initialTable
//     }
    
//     func run() {
//         let parser = CommandLineArgumentsParser()
//         let arguments = parser.parseCommandLineArguments()
        
//         for argument in arguments {
//             if let option = argument.option, let value = argument.value {
//                 switch option {
//                 case "add-row":
//                     let rowValues = value.components(separatedBy: ",")
//                     table.append(rowValues)
//                 case "remove-row":
//                     if let rowIndex = Int(value) {
//                         table.remove(at: rowIndex)
//                     } else {
//                         print("Invalid remove-row argument. Expected integer value.")
//                     }
//                 case "add-column":
//                     let columnValues = value.components(separatedBy: ",")
//                     for (index, columnValue) in columnValues.enumerated() {
//                         if index < table.count {
//                             table[index].append(columnValue)
//                         } else {
//                             print("add-column: Number of values exceeds number of rows.")
//                             break
//                         }
//                     }
//                 case "remove-column":
//                     if let columnIndex = Int(value) {
//                         for row in 0..<table.count {
//                             if columnIndex < table[row].count {
//                                 table[row].remove(at: columnIndex)
//                             }
//                         }
//                     } else {
//                         print("Invalid remove-column argument. Expected integer value.")
//                     }
//                 case "sort":
//                     let parts = value.components(separatedBy: ",")
//                     if parts.count == 2, let columnNumber = Int(parts.first!), let columnOrder = parts.last {
//                         let ascending = (columnOrder.lowercased() == "asc")
//                         sortTableByColumn(columnNumber: columnNumber, ascending: ascending)
//                     } else {
//                         print("Invalid sort argument format. Expected format: 'column_number,asc|desc'")
//                     }
//                 default:
//                     print("Unknown option: \(option)")
//                 }
//             }
//         }
        
//         for row in table {
//             // print(row.joined(separator: "\t"))
//             row.joined(separator: "\t")
//         }
//     }
    
//     private func sortTableByColumn(columnNumber: Int, ascending: Bool) {
//         table.sort(by: { (row1, row2) -> Bool in
//             if columnNumber < row1.count && columnNumber < row2.count {
//                 let value1 = row1[columnNumber]
//                 let value2 = row2[columnNumber]
                
//                 if let number1 = Int(value1), let number2 = Int(value2) {
//                     return ascending ? (number1 < number2) : (number1 > number2)
//                 } else {
//                     return ascending ? (value1 < value2) : (value1 > value2)
//                 }
//             } else {
//                 return false
//             }
//         })
//     }
// }
// let initialTable = [
//     ["Data1", "Data2", "Data3"],
//     ["I'm Data1 one", "I'm Data2 one", "I'm Data3 one"],
//     ["I'm Data1 two", "I'm Data2 two", "I'm Data3 two"],
//     ["I'm Data1 three", "I'm Data2 three", "I'm Data3 three"]
// ]

// func runComplexBuildInCall()->Int{
//     let timer = Timer()
//     global = 0
//     timer.start()
//     for i in 0..<MAXSIZE {
//         let processor = TableProcessor(initialTable: initialTable)
//         processor.run()
//     }
//     let time = timer.stop()
//     return Int(time)
// }
// runComplexBuildInCall()