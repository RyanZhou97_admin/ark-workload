declare function print(arg:any) : string;

declare interface ArkTools{
	timeInUs(arg:any):number
}
class Timer {
    private readonly CLOCK_REALTIME = 0;
    private  start_time = 0
  
    public start(): void {
        this.start_time = ArkTools.timeInUs();
    }
  
    public stop(): number {
      let end_time = ArkTools.timeInUs();
      const time = end_time - this.start_time;
      return time / 1000;
    }
  }
  
  function GenerateRandoms(): number[] {
    const result : number[] = [];
    for (let i = 0; i < 2; i++) {
        const randomNum: number = Math.floor(Math.random() * 2) + 1; // 生成介于1和2之间的随机整数
        result.push(randomNum);
    }
    return result;
  }
  let generaterandoms = GenerateRandoms();
  
  function TimeGenerateRandoms(): number {
    const timer = new Timer();
    timer.start();
    let tmp = 1;
    if (generaterandoms[tmp % 2] != 0) {
    }
    const time = timer.stop();
    return time;
  }

  let global_value = 0;

  function GenerateFakeRandomInteger(): Int32Array {
    let resource: Int32Array = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    return resource;
  }

  class ClassFunc {
    foo(resources: number[], i: number, i3: number, resourcesLength: number): number {
      if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
          i3 += 1;
      } else {
          i3 += 2;
      }
      return i3;
    }
    parameterlessFoo(): void {
      global_value += 1;
    }
    differentFoo(a: number, b: string, c: boolean): void {
      global_value += 1;
    }
    defaultFoo(a = 0, b = 'b', c = true): void {
      global_value += 1;
    }
    variableFoo(a?: number, b?: string, c?: boolean): void {
      global_value += 1;
    }
    argsFoo(...args: number[]): void {
      global_value += 1;
    }
  }
  
  /***************************** With parameters *****************************/
  // Method call
  function runMethodCall(): number {
    let count : number = 10000000;
    const cf = new ClassFunc();
    const timer = new Timer();
    let i3 : number = 1;
    let resources : number[] = GenerateFakeRandomInteger();
    timer.start();
    let func = cf.foo;
    let resourcesLength: number = resources.length;
    for (let i = 0; i < count; i++) {
      i3 = func(resources, i, i3, resourcesLength);
    }
    const time = timer.stop();
    print("Function Call - RunMethodCall:\t"+String(time)+"\tms");
    // print("Method Call:\t"+String(time)+"\tms");
    
    return time;
  }
  runMethodCall();
  
  function runParameterlessMethodCall(): number {
    let count : number = 10000000;
    const cf = new ClassFunc();
    const timer = new Timer();
    global_value = 0;
    timer.start();
    for (let i = 0; i < count; i++) {
      cf.parameterlessFoo();
    }
    const time = timer.stop();
    print("Function Call - RunParameterlessMethodCall:\t"+String(time)+"\tms");
    return time
  }
  runParameterlessMethodCall();
  
  /***************************** Default parameters *****************************/
  
  // Method call
  function runDefMethodCall(): number {
    let count : number = 10000000;
    const cf = new ClassFunc();
    const timer = new Timer();
    global_value = 0;
    timer.start();
    for (let i = 0; i < count; i++) {
      cf.defaultFoo();
    }
    const time = timer.stop();
    print("Function Call - RunDefMethodCall:\t"+String(time)+"\tms");
    return time
  }
  runDefMethodCall();
  
  /********************* Different parameters *********************/
  
  // Method call
  function runDifMethodCall(): number {
    let count : number = 10000000;
    const cf = new ClassFunc();
    const timer = new Timer();
    global_value = 0;
    timer.start();
    for (let i = 0; i < count; i++) {
      cf.differentFoo(1, '1', true);
    }
    const time = timer.stop();
    print("Function Call - RunDifMethodCall:\t"+String(time)+"\tms");
    return time
  }
  runDifMethodCall();
  
  /************************* Variable parameters *************************/
  
  // Method call
  function runVariableMethodCall(): number {
    let count : number = 10000000;
    const cf = new ClassFunc();
    const timer = new Timer();
    global_value = 0;
    timer.start();
    for (let i = 0; i < count; i++) {
      cf.variableFoo(1, '1', true);
    }
    const time = timer.stop();
    print("Function Call - RunVariableMethodCall:\t"+String(time)+"\tms");
    return time
  }
  runVariableMethodCall();
  
  /***************************** ...Args parameters *****************************/
  
  // Method call
  function runArgsMethodCall(): number {
    let count : number = 10000000;
    const cf = new ClassFunc();
    const timer = new Timer();
    global_value = 0;
    
    timer.start();
    for (let i = 0; i < count; i++) {
      cf.argsFoo(1);
    }
    const time = timer.stop();
    print("Function Call - RunArgsMethodCall:\t"+String(time)+"\tms");
    return time
}
// runArgsMethodCall()
print("Ts Method Call Is End, global_value value: \t" + String(global_value));