// import { BenchmarkRunner } from "../../../utils/benchmarkTsSuite";
declare function print(arg:any) : string;
declare interface ArkTools{
	timeInUs(arg:any):number
}
let global_value = 0;
class Example {
    //static With parameters
    static Foo(resources: Int32Array, i: number, i3: number, resourcesLength: number): number {
        if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
            i3 += 1;
        } else {
            i3 += 2;
        }
        return i3;
    }
    //static Without parameters
    static parameterlessFoo(){  
        global_value += 1
    }
    //static Different  parameters
    static differentFoo(a:number, b:string, c:boolean){
        global_value += 1
    }
    //static Default  parameters
    static defaultFoo(a:number=0, b:string="b", c:boolean=true){
        global_value += 1
    }
    //static Variable  parameters
    static variableFoo(a?:number, b?:string, c?:boolean){
        global_value += 1
    }
    //static ...Args  parameters
    static argsFoo(...args:number[]){
        global_value += 1
    }
}

function GenerateFakeRandomInteger(): Int32Array {
    let resource: Int32Array = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    return resource;
}

export function RunStaticFunction():number{
    let count : number = 10000000;
    let i3 : number = 1;
    let resources : Int32Array = GenerateFakeRandomInteger();
    let startTime = ArkTools.timeInUs();
    let func1 = Example.Foo;
    let resourcesLength: number = resources.length;
    for(let i=0;i<count;i++) {
        i3 = func1(resources, i, i3, resourcesLength)
    }
    let endTime = ArkTools.timeInUs();
    let time = (endTime - startTime) / 1000
    print("Static Function Call - RunNormalCall:\t"+String(time)+"\tms");
    return time
}
RunStaticFunction()
// let runner2 = new BenchmarkRunner("Static Function Call - RunNormalCall", RunStaticFunction);
// runner2.run();

export function RunParameterlessStaticFunction():number{
    let count : number = 10000000;
    global_value = 0;
    let startTime = ArkTools.timeInUs();
    for(let i=0;i<count;i++) {
        Example.parameterlessFoo()
    }
    let endTime = ArkTools.timeInUs();
    let time = (endTime - startTime) / 1000
    print("Static Function Call - RunParameterlessCall:\t"+String(time)+"\tms");
    return time
}
RunParameterlessStaticFunction()
// let runner1 = new BenchmarkRunner("Static Function Call - RunParameterlessCall", RunParameterlessStaticFunction);
// runner1.run();

export function RunNormalDifStaticFunc():number{
    let count : number = 10000000;
    global_value = 0;
    let startTime = ArkTools.timeInUs();
    for(let i=0;i<count;i++) {
        Example.differentFoo(1,"2",true)
    }
    let endTime = ArkTools.timeInUs();
    let time = (endTime - startTime) / 1000
    print("Static Function Call - RunNormalDifferentCall:\t"+String(time)+"\tms");
    return time
}
RunNormalDifStaticFunc()
// let runner3 = new BenchmarkRunner("Static Function Call - RunNormalDifferentCall", RunNormalDifStaticFunc);
// runner3.run();

export function RunNormalVariableFStaticFunc():number{
    let count : number = 10000000;
    global_value = 0;
    let startTime = ArkTools.timeInUs();
    for(let i=0;i<count;i++) {
        Example.variableFoo(1,"2",true)
    }
    let endTime = ArkTools.timeInUs();
    let time = (endTime - startTime) / 1000
    print("Static Function Call - RunNormalVariableFCall:\t"+String(time)+"\tms");
    return time
}
RunNormalVariableFStaticFunc()
// let runner4 = new BenchmarkRunner("Static Function Call - RunNormalVariableFCall", RunNormalVariableFStaticFunc);
// runner4.run();
export function RunNormalDefCall():number {
    let count : number = 10000000;
    global_value = 0;
    let startTime = ArkTools.timeInUs();
    for(let i=0;i<count;i++) {
        Example.defaultFoo()
    }
    let endTime = ArkTools.timeInUs();
    let time = (endTime - startTime) / 1000
    print("Static Function Call - RunNormalDefCall:\t"+String(time)+"\tms");
    return time
}
RunNormalDefCall()
// let runner5 = new BenchmarkRunner("Static Function Call - RunNormalDefCall", RunNormalDefCall);
// runner5.run();

export function RunNormalArgsStaticCall():number {
    let count : number = 10000000;
    global_value = 0;
    let startTime = ArkTools.timeInUs();
    for(let i=0;i<count;i++) {
        Example.argsFoo(1,2,3,4,5,6,7,8,9,10)
    }
    let endTime = ArkTools.timeInUs();
    return (endTime - startTime) / 1000
}
// let runner6 = new BenchmarkRunner("Static Function Call - RunNormalArgsCall", RunNormalArgsStaticCall);
// runner6.run();
print("Ts Method Call Is End, global_value value: \t" + String(global_value));
