// import { BenchmarkRunner } from "../../../utils/benchmarkTsSuite";
declare function print(arg:any) : string;

declare interface ArkTools{
	timeInUs(arg:any):number
}

let global_value = 0;
/***** Without parameters *****/
function ParameterlessFoo(){
    global_value += 1
}

function GenerateFakeRandomInteger(): Int32Array {
    let resource: Int32Array = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    return resource;
}

/***************** With parameters *****************/
function Foo(resources: Int32Array, i: number, i3: number, resourcesLength: number): number {
    if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
        i3 += 1;
    } else {
        i3 += 2;
    }
    return i3;
}
export function RunNormalCall():number {
    let count : number = 10000000;
    let i3 : number = 1;
    let resources : Int32Array = GenerateFakeRandomInteger();
    let startTime = ArkTools.timeInUs();
    let foo = Foo;
    let resourcesLength: number = resources.length;
    for(let i=0;i<count;i++) {
        i3 = foo(resources, i, i3, resourcesLength);
    }
    let endTime = ArkTools.timeInUs();
    let time = (endTime - startTime) / 1000
    print("Function Call - RunNormalCall:\t"+String(time)+"\tms");
    return time
}
RunNormalCall()

// No parameter function call
export function RunParameterlessCall():number{
    let count : number = 10000000;
    global_value = 0;
    let startTime = ArkTools.timeInUs();
    for(let i=0;i<count;i++) {
        ParameterlessFoo()
    }
    let endTime = ArkTools.timeInUs();
    let time = (endTime - startTime) / 1000
    print("Function Call - RunParameterlessCall:\t"+String(time)+"\tms");
    return time
}
RunParameterlessCall()

/***************************** Default  parameters *****************************/
function DefaultFoo(a:number=0, b:string="b", c:boolean=true) {
    global_value += 1
}
export function RunNormalDefCall():number {
    let count : number = 10000000;
    global_value = 0;
    let startTime = ArkTools.timeInUs();
    for(let i=0;i<count;i++) {
        DefaultFoo()
    }
    let endTime = ArkTools.timeInUs();
    let time = (endTime - startTime) / 1000
    print("Function Call - RunNormalDefCall:\t"+String(time)+"\tms");
    return time;
}
RunNormalDefCall()

/********************* Different  parameters *********************/
function DifferentFoo(a:number, b:string, c:boolean) {
    global_value += 1
}
export function RunNormalDifCall():number {
    let count : number = 10000000;
    global_value = 0;
    let startTime = ArkTools.timeInUs();
    for(let i=0;i<count;i++) {
        DifferentFoo(1,"1",true)
    }
    let endTime = ArkTools.timeInUs();
    let time = (endTime - startTime) / 1000
    print("Function Call - RunNormalDifferentCall:\t"+String(time)+"\tms");
    return time;
}
RunNormalDifCall()

/************************* Variable  parameters *************************/
function VariableFoo(a?:number, b?:string, c?:boolean) {
    global_value += 1
}
export function RunNormalVariableFCall():number {
    let count : number = 10000000;
    global_value = 0;
    let startTime = ArkTools.timeInUs();
    for(let i=0;i<count;i++) {
        VariableFoo(1,"1",true)
    }
    let endTime = ArkTools.timeInUs();
    let time = (endTime - startTime) / 1000
    print("Function Call - RunNormalVariableFCall:\t"+String(time)+"\tms");
    return time;
}
RunNormalVariableFCall()

print("Ts Method Call Is End, global_value value: \t" + String(global_value));

