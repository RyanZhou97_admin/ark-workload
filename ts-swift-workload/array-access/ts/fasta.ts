// import { BenchmarkRunner } from "../../../utils/benchmarkTsSuite";
declare function print(arg:any) : string;
declare interface ArkTools{
	timeInUs(arg:any):number
}
// Modify n value to change the difficulty of the benchmark
// suggesting using input = 25000000;
const input: number = 250000;

class AminoAcid {
    prob: number = 0.0;
    sym: number = 0;
    constructor(prob: number, sym: number) {
        this.prob = prob;
        this.sym = sym;
    }
}

const IM = 139968;
const IA = 3877;
const IC = 29573;
let seed = 42;

let n: number = input;


const width: number = 60;

const aluString: string =
    "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG" +
    "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA" +
    "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT" +
    "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA" +
    "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG" +
    "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC" +
    "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA";
let alu = new Array(aluString.length);
for (let i = 0; i < aluString.length; i++) {
    alu[i] = aluString[i].charCodeAt(0);
}
alu.pop();

const iub: AminoAcid[] = [
    new AminoAcid(0.27, 97), // "a"),
    new AminoAcid(0.12, 99), // "c"),
    new AminoAcid(0.12, 103), // "g"),
    new AminoAcid(0.27, 116), // "t"),
    new AminoAcid(0.02, 66), // "B"),
    new AminoAcid(0.02, 68), // "D"),
    new AminoAcid(0.02, 72), // "H"),
    new AminoAcid(0.02, 75), // "K"),
    new AminoAcid(0.02, 77), // "M"),
    new AminoAcid(0.02, 78), // "N"),
    new AminoAcid(0.02, 82), // "R"),
    new AminoAcid(0.02, 83), // "S"),
    new AminoAcid(0.02, 86), // "V"),
    new AminoAcid(0.02, 87), // "W"),
    new AminoAcid(0.02, 89), // "Y"),
];



function repeatFasta(geneLength: number, buffer: number[], gene2: number[]) : number {
    let pos = 0;
    let rpos = 0;
    let cnt = 500000;
    let lwidth = width;

    while (cnt > 0) {
        if (pos + lwidth > buffer.length) {
            pos = 0;
        }
        if (rpos + lwidth > geneLength) {
            rpos = rpos % geneLength;
        }
        if (cnt < lwidth) {
            lwidth = cnt;
        }
        for (let i = 0; i < lwidth; i++) {
            buffer[pos + i] = gene2[rpos + i];
        }

        buffer[pos + lwidth] = 10;
        pos += lwidth + 1;
        rpos += lwidth;
        cnt -= lwidth;
    }
    if (pos > 0 && pos < buffer.length) {
        buffer[pos] = 10;
    } else if (pos === buffer.length) {
        buffer[0] = 10;
    }

    let result: number = 0;
    for (let i = 0; i < buffer.length; i++) {
        result += buffer[i];
    }
    return result;
}

function search(rnd: number, arr: AminoAcid[]): number {
    let low = 0;
    let high = arr.length - 1;

    while (low <= high) {
        const mid = low + Math.floor((high - low) / 2);
        if (arr[mid].prob >= rnd) {
            high = mid - 1;
        } else {
            low = mid + 1;
        }
    }

    return arr[high + 1].sym;
}

function binarySearch(rnd: number, arr: AminoAcid[]): number {
    let low = 0;
    let high = arr.length - 1;
    let middle = high >>> 1;
    for (; low <= high; middle = (low + high) >>> 1) {
        if (arr[middle].prob >= rnd) {
            high = middle - 1;
        } else {
            low = middle + 1;
        }
    }
    return arr[high + 1].sym;
}

function accumulateProbabilities(acid: AminoAcid[]): void {
    for (let i = 1; i < acid.length; i++) {
        acid[i].prob += acid[i-1].prob;
    }
}

function randomFasta(acid: AminoAcid[], n: number) {
    const bufferSize: number = 256 * 1024;
    let cnt = n;
    accumulateProbabilities(acid);
    let buffer = new Array(bufferSize).fill(10);
    let pos = 0;
    while (cnt > 0) {
        let m = cnt > width ? width : cnt;
        let f = 1.0 / IM;
        let myrand = seed;
        for (let i = 0; i < m; i++) {
            myrand = (myrand * IA + IC) % IM;
            let r = myrand * f;
            buffer[pos] = binarySearch(r, acid);
            pos++;
            if (pos === buffer.length) {
                pos = 0;
            }
        }
        seed = myrand;
        buffer[pos] = 10;
        pos++;
        if (pos === buffer.length) {
            pos = 0;
        }
        cnt -= m;
    }
}

function RunFastaRepeat() {
    const bufferSize: number = 256 * 1024;
    let buffer = new Array();
    for (let i = 0; i < bufferSize; i++) {
        buffer.push(10);
    }
    let geneLength: number = alu.length;
    let gene2 = new Array(2 * geneLength);
	for (let i = 0; i < gene2.length; i++) {
		gene2[i] = alu[i % geneLength];
	}
    let res: number = 0;
    let start = ArkTools.timeInUs();
    for (let i = 0; i < 500; i++) {
        res += repeatFasta(geneLength, buffer, gene2);
    }
    let end = ArkTools.timeInUs();
    let time = (end - start) / 1000
    print(res);
    print("Array Access - RunFastaRepeat:\t"+String(time)+"\tms");
	return time;
}
RunFastaRepeat()
// let runner1 = new BenchmarkRunner("Array Access - RunFastaRepeat", RunFastaRepeat);
// runner1.run();

export function RunFastaRandom1() {
    let start = ArkTools.timeInUs();
    randomFasta(iub, 3*n);
    let end = ArkTools.timeInUs();
    let time = (end - start) / 1000
    print("Array Access - RunFastaRandom1:\t"+String(time)+"\tms");
	return time;
}
RunFastaRandom1()
// let runner2 = new BenchmarkRunner("Array Access - RunFastaRandom1", RunFastaRandom1);
// runner2.run();

export function RunFastaRandom2() {
    const homosapiens: AminoAcid[] = [
        new AminoAcid(0.3029549426680, 97), // "a"),
        new AminoAcid(0.1979883004921, 99), // "c"),
        new AminoAcid(0.1975473066391, 103), // "g"),
        new AminoAcid(0.3015094502008, 116), // "t"),
    ];
    let start = ArkTools.timeInUs();
    randomFasta(homosapiens, 5*n);
    let end = ArkTools.timeInUs();
    let time = (end - start) / 1000
    print("Array Access - RunFastaRandom2:\t"+String(time)+"\tms");
	return time;
}
RunFastaRandom2()
// let runner3 = new BenchmarkRunner("Array Access - RunFastaRandom2", RunFastaRandom2);
// runner3.run();
