import Glibc

class Timer {
    private let CLOCK_REALTIME = 0
    private var start_timespec = timespec()
    private var end_timespec = timespec()
    private var time_spec = timespec()
    func start() {
        clock_gettime(Int32(CLOCK_REALTIME),&start_timespec)
    }
    
    func stop() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&end_timespec)
        let start_time = Double(start_timespec.tv_sec * 1_000_000 + start_timespec.tv_nsec / 1_000)
        let end_time = Double(end_timespec.tv_sec * 1_000_000 + end_timespec.tv_nsec / 1_000)
        let time = end_time - start_time
        return time / 1_000
    }
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

let timer = Timer()

typealias AminoAcid = (prob: Double, sym: UInt8)

let IM = 139968
let IA = 3877
let IC = 29573
var seed = 42

let width = 60

let aluString = "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG" +
    "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA" +
    "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT" +
    "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA" +
    "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG" +
    "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC" +
"AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA"
var alu = aluString.utf8CString.map({ UInt8($0) })
alu.popLast()

var iub = [
    AminoAcid(0.27, 97), // "a"),
    AminoAcid(0.12, 99), // "c"),
    AminoAcid(0.12, 103), // "g"),
    AminoAcid(0.27, 116), // "t"),
    AminoAcid(0.02, 66), // "B"),
    AminoAcid(0.02, 68), // "D"),
    AminoAcid(0.02, 72), // "H"),
    AminoAcid(0.02, 75), // "K"),
    AminoAcid(0.02, 77), // "M"),
    AminoAcid(0.02, 78), // "N"),
    AminoAcid(0.02, 82), // "R"),
    AminoAcid(0.02, 83), // "S"),
    AminoAcid(0.02, 86), // "V"),
    AminoAcid(0.02, 87), // "W"),
    AminoAcid(0.02, 89), // "Y"),
]

var homosapiens = [
    AminoAcid(0.3029549426680, 97), // "a"),
    AminoAcid(0.1979883004921, 99), // "c"),
    AminoAcid(0.1975473066391, 103), // "g"),
    AminoAcid(0.3015094502008, 116), // "t"),
]

func repeatFasta(geneLength: Int, buffer: inout [Int], gene2: inout [Int]) -> Int {
    var pos = 0
    var rpos = 0
    var cnt = 500000
    var lwidth = width
    while cnt > 0 {
        if pos + lwidth > buffer.count {
            pos = 0
        }
        if rpos + lwidth > geneLength {
            rpos = rpos % geneLength
        }
        if cnt < lwidth {
            lwidth = cnt
        }
        
        for i in 0..<lwidth {
            buffer[pos + i] = gene2[rpos + i];
        }

        buffer[pos+lwidth] = 10
        pos += lwidth + 1
        rpos += lwidth
        cnt -= lwidth
    }
    if pos > 0 && pos < buffer.count {
        buffer[pos] = 10
    } else if pos == buffer.count {
        buffer[0] = 10
    }
    var result = 0
    for i in 0..<buffer.count {
        result += buffer[i]
    }
    return result
}

func search(rnd: Double, within arr: [AminoAcid]) -> UInt8 {
    var low = 0
    var high = arr.count - 1
    while low <= high {
        let mid = (high + low) >> 1
        if arr[mid].prob >= rnd {
            high = mid - 1
        } else {
            low = mid + 1
        }
    }
    return arr[high+1].sym
}

func accumulateProbabilities( acid: inout [AminoAcid]) {
    for i in 1..<acid.count {
        acid[i].prob += acid[i-1].prob
    }
}

func randomFasta( acid: inout [AminoAcid], _ n: Int) {
    let bufferSize = 256*1024
    var cnt = n
    accumulateProbabilities(acid: &acid)
    var buffer = [UInt8](repeating: 10, count: bufferSize)
    var pos = 0
    while cnt > 0 {
        var m = cnt
        if m > width {
            m = width
        }
        let f = 1.0 / Double(IM)
        var myrand = seed
        for _ in 0..<m {
            myrand = (myrand * IA + IC) % IM
            let r = Double(myrand) * f
            buffer[pos] = search(rnd: r, within: acid)
            pos += 1
            if pos == buffer.count {
                pos = 0
            }
        }
        seed = myrand
        buffer[pos] = 10
        pos += 1
        if pos == buffer.count {
            pos = 0
        }
        cnt -= m
    }
}

func runFasta1()->Int{
    let bufferSize = 256 * 1024
    let geneLength = alu.count
    var buffer = [Int](repeating: 10, count: bufferSize)
    var gene2 = [Int](repeating: 10, count: (2 * geneLength))
	for i in 0..<gene2.count {
		gene2[i] = Int(alu[i % geneLength]);
	}
    var res = 0
    timer.start()
    for _ in 0..<500 {
        res += repeatFasta(geneLength: geneLength, buffer: &buffer, gene2: &gene2)
    }
    let time = timer.stop()
    print(res)
    print("Array Access - RunFastaRepeat:\t" + String(time) + "\tms");
    return  Int(time) 
}
_ = runFasta1()

/*
func runFasta2()->Int{
    let n: Int = 250000
    // alu.popLast()
    timer.start()
    randomFasta(acid: &iub,3*n)
    let time = timer.stop()
    print("Array Access - RunFastaRandom1:\t" + String(time) + "\tms");
    return  Int(time) 
}
_ = runFasta2()
func runFasta3()->Int{
    let n: Int = 250000
    // alu.popLast()
    timer.start()
    randomFasta(acid: &homosapiens,5*n)
    let time = timer.stop()
    print("Array Access - RunFastaRandom2:\t" + String(time) + "\tms");
    return  Int(time) 
}
_ = runFasta3()
*/