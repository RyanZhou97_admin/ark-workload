import Glibc
// import Foundation


class Timer {
    private let CLOCK_REALTIME = 0
    private var start_timespec = timespec()
    private var end_timespec = timespec()
    private var time_spec = timespec()
    func start() {
        clock_gettime(Int32(CLOCK_REALTIME),&start_timespec)
    }
    
    func stop() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&end_timespec)
        let start_time = Double(start_timespec.tv_sec * 1_000_000 + start_timespec.tv_nsec / 1_000)
        let end_time = Double(end_timespec.tv_sec * 1_000_000 + end_timespec.tv_nsec / 1_000)
        let time = end_time - start_time
        return time / 1_000
    }
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

let timer = Timer()

typealias AminoAcid = (prob: Double, sym: UInt8)

let IM = 139968
let IA = 3877
let IC = 29573
var seed = 42

let n: Int = 25000000

let bufferSize = 256*1024
let width = 60

let aluString = "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG" +
    "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA" +
    "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT" +
    "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA" +
    "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG" +
    "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC" +
"AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA"

var iub = [
    AminoAcid(0.27, 97), // "a"),
    AminoAcid(0.12, 99), // "c"),
    AminoAcid(0.12, 103), // "g"),
    AminoAcid(0.27, 116), // "t"),
    AminoAcid(0.02, 66), // "B"),
    AminoAcid(0.02, 68), // "D"),
    AminoAcid(0.02, 72), // "H"),
    AminoAcid(0.02, 75), // "K"),
    AminoAcid(0.02, 77), // "M"),
    AminoAcid(0.02, 78), // "N"),
    AminoAcid(0.02, 82), // "R"),
    AminoAcid(0.02, 83), // "S"),
    AminoAcid(0.02, 86), // "V"),
    AminoAcid(0.02, 87), // "W"),
    AminoAcid(0.02, 89), // "Y"),
]

var homosapiens = [
    AminoAcid(0.3029549426680, 97), // "a"),
    AminoAcid(0.1979883004921, 99), // "c"),
    AminoAcid(0.1975473066391, 103), // "g"),
    AminoAcid(0.3015094502008, 116), // "t"),
]

func repeatFasta(gene: [UInt8], n: Int) {
    let gene2 = gene + gene
    var buffer = [UInt8](repeating: 10, count: bufferSize)
    var pos = 0
    var rpos = 0
    var cnt = n
    var lwidth = width
    while cnt > 0 {
        if pos + lwidth > buffer.count {
            pos = 0
        }
        if rpos + lwidth > gene.count {
            rpos = rpos % gene.count
        }
        if cnt < lwidth {
            lwidth = cnt
        }
        buffer[pos..<pos+lwidth] = gene2[rpos..<rpos+lwidth]
        buffer[pos+lwidth] = 10
        pos += lwidth + 1
        rpos += lwidth
        cnt -= lwidth
    }
    if pos > 0 && pos < buffer.count {
        buffer[pos] = 10
    } else if pos == buffer.count {
        buffer[0] = 10
    }
}

func search(rnd: Double, within arr: [AminoAcid]) -> UInt8 {
    var low = 0
    var high = arr.count - 1
    while low <= high {
        let mid = low + (high - low) / 2
        if arr[mid].prob >= rnd {
            high = mid - 1
        } else {
            low = mid + 1
        }
    }
    return arr[high+1].sym
}

func accumulateProbabilities( acid: inout [AminoAcid]) {
    for i in 1..<acid.count {
        acid[i].prob += acid[i-1].prob
    }
}

func randomFasta( acid: inout [AminoAcid], _ n: Int) {
    var cnt = n
    accumulateProbabilities(acid: &acid)
    var buffer = [UInt8](repeating: 10, count: bufferSize)
    var pos = 0
    while cnt > 0 {
        var m = cnt
        if m > width {
            m = width
        }
        let f = 1.0 / Double(IM)
        var myrand = seed
        for _ in 0..<m {
            myrand = (myrand * IA + IC) % IM
            let r = Double(myrand) * f
            buffer[pos] = search(rnd: r, within: acid)
            pos += 1
            if pos == buffer.count {
                pos = 0
            }
        }
        seed = myrand
        buffer[pos] = 10
        pos += 1
        if pos == buffer.count {
            pos = 0
        }
        cnt -= m
    }
}

func runFasta1()->Int{
    var alu = aluString.utf8CString.map({ UInt8($0) })
    _ = alu.popLast()
    timer.start()
    repeatFasta(gene: alu, n: 2*n)
    let time = timer.stop()
    return Int(time)
}
func runFasta2()->Int{
    var alu = aluString.utf8CString.map({ UInt8($0) })
    _ = alu.popLast()
    timer.start()
    randomFasta(acid: &iub,3*n)
    let time = timer.stop()
    return Int(time)
}
func runFasta3()->Int{
    var alu = aluString.utf8CString.map({ UInt8($0) })
    _ = alu.popLast()
    timer.start()
    randomFasta(acid: &homosapiens,5*n)
    let time = timer.stop()
    return Int(time)
}
print("fasta1:\t\(runFasta1())\tms")
print("fasta2:\t\(runFasta2())\tms")
print("fasta3:\t\(runFasta3())\tms")