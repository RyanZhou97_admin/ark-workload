// @ts-nocheck
// Modify n value to change the difficulty of the benchmark
// suggesting using input = 16000;
const input: number = 16000;

declare interface ArkTools {
	timeInUs(arg:any):number
}

function main() {

	const w: number = input;
	const h = w;

	let bit_num: number = 0;
	let i: number = 0;
	let byte_acc: number = 0;
	const iter: number = 50;
	const limit = 2.0;
	let Zr: number = 0;
	let Zi: number = 0;
	let Cr: number = 0;
	let Ci: number = 0;
	let Tr: number = 0;
	let Ti: number = 0;

	for (let y = 0; y < h; y++) {
		for (let x = 0; x < w; x++) {
			Zr = 0.0;
			Zi = 0.0;
			Tr = 0.0;
			Ti = 0.0;
			Cr = 2.0 * x / w - 1.5;
			Ci = 2.0 * y / h - 1.0;

			i = 0;
			while (i < iter && (Tr + Ti <= limit * limit)) {
				i += 1;
				Zi = 2.0 * Zr * Zi + Ci;
				Zr = Tr - Ti + Cr;
				Tr = Zr * Zr;
				Ti = Zi * Zi;
			}

			byte_acc <<= 1;
			if ((Tr + Ti) <= (limit * limit)) {
				byte_acc |= 0x01;
			}

			bit_num += 1;

			if (bit_num == 8) {
				byte_acc = 0;
				bit_num = 0;
			} else if (x == (w - 1)) {
				byte_acc <<= (8 - w % 8);
				byte_acc = 0;
				bit_num = 0;	
			}
		}
	}
}

let start = ArkTools.timeInUs();
main();
let end = ArkTools.timeInUs();
print("Benchmark - TS - mandelbrot - time is " + (end - start) / 1000);
