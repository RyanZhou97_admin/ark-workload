//
//  CustomObjectCpp.hpp
//  crossLanguage
//
//  Created by Yao Yuan on 18/4/2023.
//

#ifndef CustomObjectCpp_hpp
#define CustomObjectCpp_hpp

#include <stdio.h>
#include <string>

class CustomObjectCpp
{
public:
    CustomObjectCpp() = default;
    ~CustomObjectCpp();
    
    std::string value1;
    std::string value2;
    std::string value3;
};

class DemoClass
{
public:
    DemoClass() = default;
    ~DemoClass();
    
    void AddOne() {
        value_++;
    }
private:
    double value_;
};

#endif /* CustomObjectCpp_hpp */
