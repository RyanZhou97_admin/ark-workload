//
//  crossLanguageApp.swift
//  crossLanguage
//
//  Created by Yao Yuan on 6/4/2023.
//

import SwiftUI

@main
struct crossLanguageApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
