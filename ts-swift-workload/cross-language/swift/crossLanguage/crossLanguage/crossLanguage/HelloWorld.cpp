//
//  HelloWorld.cpp
//  crossLanguage
//
//  Created by Yao Yuan on 6/4/2023.
//

#include "HelloWorld.hpp"

void HelloWorld::ExecuteCallback(callback_t callback) {
    callback();
}

std::string HelloWorld::sayHello()
{
    return "Hello from CPP world!";
}

void HelloWorld::BenchmarkZero(){}

void HelloWorld::BenchmarkOne(int64_t a){}

void HelloWorld::BenchmarkTwo(int64_t a, int64_t b){}
void HelloWorld::BenchmarkThree(int64_t a, int64_t b, int64_t c){}
void HelloWorld::BenchmarkFour(int64_t a, int64_t b, int64_t c, int64_t d){}
void HelloWorld::BenchmarkFive(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e){}
void HelloWorld::BenchmarkSix(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e, int64_t f){}
void HelloWorld::BenchmarkSeven(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e, int64_t f, int64_t g){}
void HelloWorld::BenchmarkEight(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e, int64_t f, int64_t g, int64_t h){}
void HelloWorld::BenchmarkNine(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e, int64_t f, int64_t g, int64_t h, int64_t i){}
void HelloWorld::BenchmarkTen(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e, int64_t f, int64_t g, int64_t h, int64_t i, int64_t j){}
void HelloWorld::BenchmarkObjectOne(CustomObjectCpp* a){}
void HelloWorld::BenchmarkObjectTwo(CustomObjectCpp* a, CustomObjectCpp* b){}
void HelloWorld::BenchmarkObjectThree(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c){}
void HelloWorld::BenchmarkObjectFour(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d){}
void HelloWorld::BenchmarkObjectFive(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d, CustomObjectCpp* e){}
void HelloWorld::BenchmarkObjectSix(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d, CustomObjectCpp* e, CustomObjectCpp* f){}
void HelloWorld::BenchmarkObjectSeven(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d, CustomObjectCpp* e, CustomObjectCpp* f, CustomObjectCpp* g){}
void HelloWorld::BenchmarkObjectEight(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d, CustomObjectCpp* e, CustomObjectCpp* f, CustomObjectCpp* g, CustomObjectCpp* h){}
void HelloWorld::BenchmarkObjectNine(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d, CustomObjectCpp* e, CustomObjectCpp* f, CustomObjectCpp* g, CustomObjectCpp* h, CustomObjectCpp* i){}
void HelloWorld::BenchmarkObjectTen(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d, CustomObjectCpp* e, CustomObjectCpp* f, CustomObjectCpp* g, CustomObjectCpp* h, CustomObjectCpp* i, CustomObjectCpp* j){}

void HelloWorld::BenchmarkSettingValue(CustomObjectCpp *object) {
    object->value1 = "2";
    object->value2 = "2";
    object->value3 = "2";
}

void HelloWorld::BenchmarkSettingLargeArray(std::vector<int> &vec, int32_t size){
    for (int32_t i = 0; i < size; ++i) {
        vec[i] = 1;
    }
}
