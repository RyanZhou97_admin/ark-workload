//
//  HelloWorld.hpp
//  crossLanguage
//
//  Created by Yao Yuan on 6/4/2023.
//

#ifndef HelloWorld_hpp
#define HelloWorld_hpp

#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include "CustomObjectCpp.hpp"

class HelloWorld {
public:
    typedef void(*callback_t)();
    void ExecuteCallback(callback_t callback);
    
    std::string sayHello();
    void BenchmarkZero();
    void BenchmarkOne(int64_t a);
    void BenchmarkTwo(int64_t a, int64_t b);
    void BenchmarkThree(int64_t a, int64_t b, int64_t c);
    void BenchmarkFour(int64_t a, int64_t b, int64_t c, int64_t d);
    void BenchmarkFive(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e);
    void BenchmarkSix(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e, int64_t f);
    void BenchmarkSeven(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e, int64_t f, int64_t g);
    void BenchmarkEight(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e, int64_t f, int64_t g, int64_t h);
    void BenchmarkNine(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e, int64_t f, int64_t g, int64_t h, int64_t i);
    void BenchmarkTen(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e, int64_t f, int64_t g, int64_t h, int64_t i, int64_t j);
    void BenchmarkObjectOne(CustomObjectCpp* a);
    void BenchmarkObjectTwo(CustomObjectCpp* a, CustomObjectCpp* b);
    void BenchmarkObjectThree(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c);
    void BenchmarkObjectFour(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d);
    void BenchmarkObjectFive(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d, CustomObjectCpp* e);
    void BenchmarkObjectSix(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d, CustomObjectCpp* e, CustomObjectCpp* f);
    void BenchmarkObjectSeven(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d, CustomObjectCpp* e, CustomObjectCpp* f, CustomObjectCpp* g);
    void BenchmarkObjectEight(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d, CustomObjectCpp* e, CustomObjectCpp* f, CustomObjectCpp* g, CustomObjectCpp* h);
    void BenchmarkObjectNine(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d, CustomObjectCpp* e, CustomObjectCpp* f, CustomObjectCpp* g, CustomObjectCpp* h, CustomObjectCpp* i);
    void BenchmarkObjectTen(CustomObjectCpp* a, CustomObjectCpp* b, CustomObjectCpp* c, CustomObjectCpp* d, CustomObjectCpp* e, CustomObjectCpp* f, CustomObjectCpp* g, CustomObjectCpp* h, CustomObjectCpp* i, CustomObjectCpp* j);
    void BenchmarkSettingValue(CustomObjectCpp* object);
    
    void BenchmarkSettingLargeArray(std::vector<int> &vec, int32_t size);
    
};
#endif /* HelloWorld_hpp */
